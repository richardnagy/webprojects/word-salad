# Word Salad

Browser game where you guess a 5 letter word.

## Live Demo

https://richardnagy.gitlab.io/webprojects/word-salad/

## Installation

1. Install dependencies

```bash
yarn install
```

2. Build package

```bash
yarn build
```

3. Hosting

Host the files with the built-in node server

```bash
yarn serve
```

or move the files from `./dist/` to your server.

## Development

1. Install dependencies

```bash
yarn install
```

2. Start services

```bash
yarn dev
```